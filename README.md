# EEE3088F_PiHat_Project

PiHat Project for EEE3088F
1. The PiHat will sense if it is day or night, and depending on which day it is, it will either send a signal to a security system to be armed, or will send a signal to disarm. The PiHat will also have 3 status LED's that will display if it is day or night, if the system is armed or disarmed and if the PiHat is being powered. What is currently included is a SPICE simpulation file for the light sensor part of the PiHat.
2. To use the SPICE file, a applicable SPICE software must be used, the file was make using LTSpice. LTSpice is freee software and the link to download it is: https://www.analog.com/en/design-center/design-tools-and-calculators/ltspice-simulator.html.
3. Using SPICE software is a bit tricky without prior knowledge, here is a Youtube video explaining the basics of LTSpice: https://www.youtube.com/watch?v=ih-kHeMhEbA
4. This circuit design could be used to control an electric fence around a school, the fence shouldn't be activated during the day to prefevnt the case of a child accidentally shocking themselves. It could automatically turn on the fence at night when there are no children around.
5. Anyone can use the simulations contained here.
