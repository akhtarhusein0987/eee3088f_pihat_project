This uHAT is designed to be used with the Raspberry Pi Zero, the uHAT connects to the pins of the Pi.
The uHAT contains a voltage regulator subsystem, a light sensor subsystem and a LED display susbsystem.
The voltage regulator subsystem is powered by an external voltage in the range of 11.5V to 12.5V and it powers the uHAT and the Raspberry Pi.
The light sensor will send a voltage of between 0V and 5V to GPIO_19 (Pin 35) of the Pi.
This voltage will then have to be interpreted by the Pi to represent either day or night.
This voltage is controlled by an LDR and a potentiometer connected to SDA_1 (Pin 3) and SCL_1 (Pin 5), by varying the resistance of the potentiometer you can set the sensitivity to light of the sensor.
The display LED's are connected to GPIO_17 (Pin 11), GPIO_27 (Pin 13) and GPIO_22 (Pin 15) and by outputting a voltage from those pins, corresponding display LED's will light up.
The intended use of the uHAT is to act as a switch for a security system that only turns on at night.
This uHAT is designed to send a voltage to the Pi that increases at the day gets darker, the Pi will then interpret the signal and decide if it's day or night and then send power to the appropriate LED GPIO depending on its interpetation.
This uHAT does not power and is not intended to be connected to a security system, the Pi should be connected to the system and send a signal for the system to be on or off depending of whether it's day or night.
 